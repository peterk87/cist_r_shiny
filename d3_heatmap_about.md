#### About the D3 Heatmap

This is an interactive heatmap using the <a href="http://d3js.org/" target="D3">D3 JavaScript library</a>.

- *Hover* over cells to view the underlying metadata for the strain to strain comparison 
- *Click* on a cell to lock or unlock the strain to strain comparison data table
- Zoom with the *mousewheel* 
- Pan by *clicking and dragging* the heatmap around

**This visualization is currently limited to matrices with a maximum size of 300x300. Larger matrices may crash your browser due to the memory and CPU requirements.**