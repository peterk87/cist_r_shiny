
output$categories1 <- renderUI({
  print('output$categories1')
  cols <- data_col_names(metadata)
  tagList(
    selectInput("category1", 
      "Choose a category:",  
      choices=cols,
      selected=names(cols)[1],
      multiple=FALSE),
    tags$script('$("#category1").chosen({width: "200px"});')
  )
})

output$groups1 <- renderUI({
  print('output$groups1')
  if (is.null(input$category1)) return()
  gs <- get_cat_group_names(metadata, input$category1)
  cat('groups1 categoryGroups1 gs: ', gs, '\n')
  selectInput("categoryGroups1", 
    "Specify one or more groups:", 
    choices=gs, 
    selected=names(gs)[1:10], 
    multiple=TRUE
  )
})

output$categories2 <- renderUI({
  print('output$categories2')
  if (is.null(values$metadata_subset)) return()
  if (is.null(input$categoryGroups1)) return()
  cols <- data_col_names(values$metadata_subset)

  if (is.null(input$category2))
  {
    # by default select the first category in the list of categories
    # values$selected_category2 <- ''
    values$selected_category2 <- names(cols)[3]
  }
  else
  {
    # otherwise try to preserve the user's last selection
    values$selected_category2 <- names(cols[cols == input$category2])
  }
  tagList(
    selectInput("category2", 
      "Choose a category to color by:",  
      choices=c(None='', cols), 
      selected=values$selected_category2,
      multiple=FALSE),
    tags$script('$("#category2").chosen({width: "200px"});')
  )
})

output$groups2 <- renderUI({
  print('output$groups2')
  if (is.null(input$category2)) return()
  print(':: category2')
  if (input$category2 == '') return()
  if (length(values$strains) == 0) return()

  print('output$groups2::renderUI')
  cat('nrow(values$metadata_subset): ', nrow(values$metadata_subset), '\n')
  gs <- get_cat_group_names(values$metadata_subset, input$category2)
  if (length(gs) == 0) return()
  if (is.null(input$categoryGroups2))
  {
    values$selected_categoryGroups2 <- gs[1:10]
    values$old_selected_category2 <- input$category2
  }
  else
  {
    if (values$old_selected_category2 == input$category2 & any(values$selected_categoryGroups2 %in% gs))
    {
      values$selected_categoryGroups2 <- gs[gs %in% values$selected_categoryGroups2]
    }
    else
    {
      values$selected_categoryGroups2 <- gs[1:10]
      values$old_selected_category2 <- input$category2
    }
  }
  cat('names(values$selected_categoryGroups2): ', names(values$selected_categoryGroups2), '\n')
  selectInput("categoryGroups2", 
    "Specify one or more groups:", 
    choices=gs, 
    selected=names(values$selected_categoryGroups2), 
    multiple=TRUE)
})

output$crosstab_heatmap <- renderTable({
  print('crosstab_heatmap start')
  if (is.null(values$metadata_subset)) return()
  print(':: metadata_subset')
  if (is.null(input$category1)) return()
  print('isolate category1')
  if (is.null(input$categoryGroups1)) return()
  print('isolate categoryGroups1')
  if (length(input$categoryGroups1) == 0) return()
  print('isolate length categoryGroups1')
  if (is.null(input$category2)) return()
  print('isolate category2')
  if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  print('isolate length categoryGroups2')
  if (is.null(values$mat_core_subset)) return()

  df <- values$metadata_subset
  if (input$category2 == '')
  {
    mytable <- table(as.vector(df[[input$category1]]))
  }
  else
  {
    mytable <- table(as.vector(df[[input$category1]]), as.vector(df[[input$category2]]))
  }
  return(mytable)

})

output$show_num_strains <- renderText({
  print('show_num_strains start')
  if (is.null(input$category1)) return()
  if (is.null(input$categoryGroups1)) return()
  if (length(input$categoryGroups1) == 0) return()
  if (!is.null(input$category2)) 
  {
    if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  }

  print('show_num_strains checks passed:')
  cat("input$categoryGroups1", input$categoryGroups1, '\n')
  bool_catA <- metadata[[input$category1]] %in% input$categoryGroups1
  cat('any(bool_catA)', any(bool_catA), '\n')
  if ('N/A' %in% input$categoryGroups1)
  {
    catA_na <- is.na(metadata[[input$category1]])
    bool_catA <- bool_catA | catA_na
  }

  bool_catB <- bool_catA
  cat('any(bool_catB)', any(bool_catB), '\n')
  cat('any(bool_catB & bool_catA)', any(bool_catB & bool_catA), '\n')
  catB_types <- NULL
  catB_colors <- NULL
  colors_catB_types <- NULL

  strains <- rownames(metadata)
  strains <- strains[bool_catA]
  mat_core_subset <- values$mat_dm_core[strains,strains]
  cat('length(strains):', length(strains), '\n')
  cat('nrow(mat_core_subset):', nrow(mat_core_subset), '\n')
  md_ordered_subset <- metadata[strains,]
  cat('nrow(md_ordered_subset): ', nrow(md_ordered_subset), '\n')

  if (!is.null(input$category2))
  {
  if (input$category2 != '')
  {
    cat('input$category2:', input$category2, '\n')
    # bool_catB <- md_ordered_subset[[input$category2]] %in% input$categoryGroups2
    # if ('N/A' %in%  input$categoryGroups2)
    # {
    #   catB_na <- is.na(md_ordered_subset[[input$category2]])
    #   bool_catB <- bool_catB | catB_na
    # }
    # cat('any(bool_catB & bool_catA)', any(bool_catB & bool_catA), '\n')
    catB_types <- input$categoryGroups2
    catB_colors <- qualitative_colours(length(input$categoryGroups2))
    colors_catB_types <- as.vector(md_ordered_subset[[input$category2]])
    colors_catB_types[!(colors_catB_types %in% input$categoryGroups2)] <- '#00000000'
    count <- 1
    while (count <= length(catB_types))
    {
      colors_catB_types[colors_catB_types == catB_types[count]] <- catB_colors[count]

      count <- count + 1
    }

    cat('colors_catB_types: ', colors_catB_types, '\n')
  }
  }

  values$types <- catB_types
  values$colors <- catB_colors
  values$colors_catB_types <- as.vector(colors_catB_types)
  cat('values$colors_catB_types: ', values$colors_catB_types, '\n')
  values$strains <- strains
  values$mat_core_subset <- mat_core_subset
  values$metadata_subset <- md_ordered_subset
    
  return(paste0('Strains selected: ', length(strains), ' of ', nrow(metadata)))
})


plot_heatmap <- function(log_scale) 
{
  print('plot_heatmap start')

  mat_core <- values$mat_core_subset
  if (is.null(mat_core)) return()
  if (length(mat_core) < 2) return()
  if (log_scale)
  {
    mat_core <- log10(mat_core)
    mat_core[mat_core == -Inf] <- 0
  }
  labRow <- NA
  if (nrow(mat_core) <= 100)
  {
    labRow <- rownames(mat_core)
  }
  if (is.null(input$category2))
  {
    heatmap.2(mat_core, trace='none', dendrogram='none', Rowv=NA, Colv=NA, labRow=labRow, labCol='', col=spectral_colors(256), breaks=0:256/256*values$max_core_distance, margins=c(5, 15), useRaster=TRUE)
  }
  else if (input$category2 == '')
  {
    heatmap.2(mat_core, trace='none', dendrogram='none', Rowv=NA, Colv=NA, labRow=labRow, labCol='', col=spectral_colors(256), breaks=0:256/256*values$max_core_distance, margins=c(5, 15), useRaster=TRUE)
  }
  else
  {
    cat('plot_heatmap colors_catB_types: ', values$colors_catB_types, '\n')
    heatmap.2(mat_core, trace='none', dendrogram='none', Rowv=NA, Colv=NA, labRow=labRow, labCol='', col=spectral_colors(256), breaks=0:256/256*values$max_core_distance, RowSideColors=values$colors_catB_types, margins=c(5, 15), useRaster=TRUE)
    legend('topright', fill=values$colors, legend=values$types, horiz=FALSE, cex=1, ncol=ceiling(length(values$types)/8), title=as.character(input$category2), border=rgb(1,1,1,0))
  }
}

# output$plot_heatmap_legend <- renderPlot({
#   plot.new()
#   # plot(1)
#   legend('left', fill=values$colors, legend=values$types, horiz=FALSE, cex=1, ncol=ceiling(length(values$types)/10), title=as.character(input$category2), border=rgb(1,1,1,0))
# })



output$label_categories_ui <- renderUI({
  print('output$label_categories_ui')
  cols <- data_col_names(metadata)
  cols <- c('Strain'='row.names', cols)
  selectInput("label_category", 
    "Choose a label category:",  
    choices=cols,
    selected=names(cols)[1],
    multiple=FALSE)
})


output$nj_tree_plot_svg <- reactive({
  print('output$nj_tree_plot start')
  if (is.null(values$mat_core_subset)) return()
  print(':: mat_core_subset')
  if (is.null(values$metadata_subset)) return()
  print(':: metadata_subset')
  input$categoryGroups2
  if (is.null(input$category1)) return()
  if (is.null(input$categoryGroups1)) return()
  if (length(input$categoryGroups1) == 0) return()
  if (!is.null(input$category2))
  {
    if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  }
  if (is.null(input$label_category)) return()

  print('plot_nj_tree_svg')
  tempsvg <- tempfile(fileext='.svg')
  print('tempsvg')
  print(tempsvg)
  svg(tempsvg)
  mat_core <- values$mat_core_subset
  if (input$label_category == 'row.names')
  {
    labels <- rownames(values$metadata_subset)
  }
  else
  {
    labels <- as.vector(values$metadata_subset[[input$label_category]])
  }
  if (input$show_missing_labels)
  {
    labels[labels == '' | is.na(labels)] <- '!MISSING'
  }
  print('labels')
  print(labels)
  nj_tree <- nj(as.dist(mat_core))
  nj_tree$tip.label <- labels
  print(nj_tree)
  tip_colors <- values$colors_catB_types
  tip_colors[tip_colors == '#00000000'] <- 'black'
  if (input$category2 == '')
  {
    plot.phylo(nj_tree, type=input$tree_type, cex=input$label_size, no.margin=TRUE)
  }
  else
  {
    plot.phylo(nj_tree, type=input$tree_type, tip.color=tip_colors, cex=input$label_size, no.margin=TRUE)
  }
  if (input$show_scale_bar)
  {
    add.scale.bar(max(mat_core)*.8, 0)
  }
  if (input$category2 != '')
  {
    legend('topright', fill=values$colors, legend=values$types, horiz=FALSE, cex=1, ncol=ceiling(length(values$types)/8), title=as.character(input$category2), border=rgb(1,1,1,0))
  }
  dev.off()
  svg_output <- readLines(tempsvg, n=-1)
  on.exit(unlink(tempsvg))
  return(svg_output)
})

plot_nj_tree <- function()
{
  print('plot_nj_tree')
  mat_core <- values$mat_core_subset
  if (input$label_category == 'row.names')
  {
    labels <- rownames(values$metadata_subset)
  }
  else
  {
    labels <- as.vector(values$metadata_subset[[input$label_category]])
  }
  if (input$show_missing_labels)
  {
    labels[labels == '' | is.na(labels)] <- '!MISSING'
  }
  print('labels')
  print(labels)
  nj_tree <- nj(as.dist(mat_core))
  nj_tree$tip.label <- labels
  print(nj_tree)
  tip_colors <- values$colors_catB_types
  tip_colors[tip_colors == '#00000000'] <- 'black'
  if (input$category2 == '')
  {
    plot.phylo(nj_tree, type=input$tree_type, cex=input$label_size, no.margin=TRUE)
  }
  else
  {
    plot.phylo(nj_tree, type=input$tree_type, tip.color=tip_colors, cex=input$label_size, no.margin=TRUE)
  }
  if (input$show_scale_bar)
  {
    add.scale.bar(max(mat_core)*.8, 0)
  }
  if (input$category2 != '')
  {
    legend('topright', fill=values$colors, legend=values$types, horiz=FALSE, cex=1, ncol=ceiling(length(values$types)/8), title=as.character(input$category2), border=rgb(1,1,1,0))
  }
}

output$nj_tree_plot <- renderPlot({
  print('output$nj_tree_plot start')
  if (is.null(values$mat_core_subset)) return()
  print(':: mat_core_subset')
  if (is.null(values$metadata_subset)) return()
  print(':: metadata_subset')
  input$categoryGroups2
  if (is.null(input$category1)) return()
  if (is.null(input$categoryGroups1)) return()
  if (length(input$categoryGroups1) == 0) return()
  if (!is.null(input$category2))
  {
    if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  }
  if (is.null(input$label_category)) return()
  plot_nj_tree()
  }, height=800, width=1000)


output$save_nj_tree_plot <- downloadHandler(
  filename = function() 
  { 
    paste0(
      'tree_nj_hamming_dist_data-', 
      input$category1, 
      '-',
      length(input$categoryGroups1), 
      '_colors-', 
      input$category2, 
      '-',
      length(input$categoryGroups2),
      '_labels-',
      input$label_category,
      '.pdf') 
  },
  content = function(file) 
  {
    pdf(file, width=input$tree_plot_save_width, height=input$tree_plot_save_height)
    plot_nj_tree()
    dev.off()
  }
)


output$heatmap_plot <- renderPlot({
  print('output$heatmap_plot start')
  if (is.null(values$mat_core_subset)) return()
  print(':: mat_core_subset')
  if (is.null(values$metadata_subset)) return()
  print(':: metadata_subset')
  input$categoryGroups2
  values$colors_catB_types
  input$log_scale
  isolate({
  if (is.null(input$category1)) return()
  print('isolate category1')
  if (is.null(input$categoryGroups1)) return()
  print('isolate categoryGroups1')
  if (length(input$categoryGroups1) == 0) return()
  print('isolate length categoryGroups1')
  if (!is.null(input$category2))
  {
    if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  }
  print('isolate category2')
  print('isolate length categoryGroups2')
  print('renderPlot heatmap_plot')
  plot_heatmap(log_scale=input$log_scale)
  })
}, height=800, width=1000)


output$save_heatmap_plot <- downloadHandler(
  filename = function() { paste('heatmap', input$category1, paste(input$categoryGroups1, sep=','), input$category2, paste(input$categoryGroups2, sep=','),'core.pdf') },
  content = function(file) 
  {
    pdf(file, width=input$plot_save_width, height=input$plot_save_height)
    plot_heatmap(log_scale=input$log_scale)
    dev.off()
  }
)


output$save_table <- downloadHandler(
  filename = function() { paste('table', input$category1, paste(input$categoryGroups1, sep=','), input$category2, paste(input$categoryGroups2, sep=','), 'crosstab.csv') },
  content = function(file) 
  {
    df <- metadata
    bool_cat1 <- df[[input$category1]] %in% input$categoryGroups1
    if ('N/A' %in% input$categoryGroups1)
    {
      bool_cat1 <- bool_cat1 | df[[input$category1]] == ''
    }
    bool_cat2 <- df[[input$category2]] %in% input$categoryGroups2
    if ('N/A' %in% input$categoryGroups2)
    {
      bool_cat2 <- bool_cat2 | df[[input$category2]] == ''
    }
    df <- subset(df, bool_cat1 & bool_cat2)
    mytable <- table(as.vector(df[[input$category1]]), as.vector(df[[input$category2]]))
    write.csv(mytable, file)
  }
)

plot_distribution <- function() 
{
  # printing to console does not seem to work with functions that 
  # produce ggplot plots
  print('plot_distribution:')
  if (is.null(input$category1)) return()
  if (is.null(input$category2)) return()
  df <- values$metadata_subset
  if (is.null(df)) 
  {
    print('values$metadata_subset is NULL')
    df <- metadata
  }
  else
  {
    cat('length(values$metadata_subset): ', length(values$metadata_subset), '\n')
    if (length(df) == 0)
    {
      df <- metadata
    }
  }
  
  # order factors by frequency for plots
  cat_1 <- as.vector(df[[input$category1]])
  # use cross tabulation function
  table_cat_1 <- as.data.frame(table(cat_1))
  # order cross tabulation table by frequency
  table_cat_1 <- table_cat_1[order(-table_cat_1$Freq),]
  # get the frequency ordered factors
  category1_ordered_levels <- table_cat_1[ , 1]
  # replace selected category with ordered factors
  df[[input$category1]] <- factor(x=cat_1, levels=category1_ordered_levels, ordered=TRUE)
  

  if (input$category2 != '')
  {
    bool_not_in_categoryGroups2 <- !(df[[input$category2]] %in% input$categoryGroups2)
    cat_2 <- as.vector(df[[input$category2]])
    cat_2[bool_not_in_categoryGroups2] <- NA
    df[[input$category2]] <- as.factor(cat_2)
  }
  
  p <- ggplot(df, aes_string(x=input$category1))
  if (input$category2 != '')
  {
    p <- p + aes_string(fill=input$category2)
    n_cat_groups <- length(input$categoryGroups2)
    p <- p + scale_fill_manual(values=qualitative_colours(n_cat_groups), na.value='gray')
  }
  p <- p + geom_histogram()
  p <- p + theme(axis.text.x=element_text(angle=45, hjust = 1))
  return(p)
}

output$visualize_distr <- renderPlot(
{
  p <- plot_distribution()
  print(p)
}, height = 700)

output$dlVizDistrPlot <- downloadHandler(
  filename = function() { 'visualize_distribution_plot.pdf' },
  content = function(file) 
  {
    pdf(file, width=input$vizDistrPlotWidth, height=input$vizDistrPlotHeight)
    plot_distribution()
    dev.off()
  }
)

output$table_metadata_distribution <- renderTable({
  print('table_metadata_distribution start')
  if (is.null(values$metadata_subset)) return()
  print(':: metadata_subset')
  if (is.null(input$category1)) return()
  print('isolate category1')
  if (is.null(input$categoryGroups1)) return()
  print('isolate categoryGroups1')
  if (length(input$categoryGroups1) == 0) return()
  print('isolate length categoryGroups1')
  if (is.null(input$category2)) return()
  print('isolate category2')
  if (input$category2 != '' & length(input$categoryGroups2) == 0) return()
  print('isolate length categoryGroups2')
  if (is.null(values$mat_core_subset)) return()

  df <- values$metadata_subset
  if (input$category2 == '')
  {
    mytable <- table(as.vector(df[[input$category1]]))
  }
  else
  {
    bool_not_in_categoryGroups2 <- !(df[[input$category2]] %in% input$categoryGroups2)
    print('df[[input$category2]] before')
    print(df[[input$category2]])
    cat_2 <- as.vector(df[[input$category2]])
    cat_2[bool_not_in_categoryGroups2] <- NA
    print('df[[input$category2]] after')
    print(cat_2)
    df[[input$category2]] <- as.factor(cat_2)
    mytable <- table(as.vector(df[[input$category1]]), as.vector(df[[input$category2]]))
  }
  return(mytable)
})

output$crosstab_explanation <- renderText({
  if (is.null(input$category1)) return()
  if (is.null(input$category2)) return()
  out_str <- ''
  if (input$category2 == '')
  {
    out_str <- paste0('Frequency of types in ', input$category1)
  }
  else
  {
    out_str <- paste0('Cross tabulation of ', input$category1, ' VS ', input$category2)
  }

  return(out_str)
})


output$mst_d3 <- reactive({
  if (is.null(values$mat_core_subset)) return()
  mat_core <- values$mat_core_subset

  hc <- hclust(as.dist(mat_core))

  g <- graph.adjacency(mat_core, weighted=TRUE)

  mst <- minimum.spanning.tree(g)
  adjList <- get.adjlist(mst)

  sources <- c()
  targets <- c()
  distances <- c()
  for (i in 1:length(adjList))
  {
    el <- adjList[i]
    links <- as.vector(unlist(el))
    if (length(links) > 0)
    {
      for (j in 1:length(links))
      {
        sources <- c(sources, i-1)
        targets <- c(targets, links[j]-1)
        distances <- c(distances, mat_core[i, links[j]])
      }
    }
  }

  groups <- values$colors_catB_types
  groups[groups == '#00000000'] <- 'black'

  links <- cbind(source=sources, target=targets, distances=distances)

  l <- list(ids=rownames(mat_core), 
    show_distances=input$show_distances,
    colours=groups, 
    groups=values$types,
    group_colours=values$colors,
    links=links, 
    max_distance=values$max_core_distance, 
    md=values$metadata_subset[,input$metadata_table_fields], 
    selected_md=input$category2,
    node_size=input$node_size,
    gravity=input$mst_gravity,
    charge=input$mst_charge,
    distance=input$mst_distance)
  # print(l)
  return(l)
})



output$ui_metadata_table_fields <- renderUI({
  cols <- data_col_names(metadata)
  tagList(
    # tags$head(tags$script('$("#metadata_table_fields").chosen({width: "300px"});')),
    selectInput("metadata_table_fields", 
      "Metadata fields to show in popup table:",  
      choices=cols,
      selected=names(cols)[1:5],
      multiple=TRUE),
    tags$script('$("#metadata_table_fields").chosen({width: "200px"});')
  )
})