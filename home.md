### CIST: the Campylobacter *In Silico* Typing server, a resource for integrated comparative genomic analysis of *Campylobacter jejuni* 

> Peter Kruczkiewicz^1,2 , Chad R. Laing^1,2 , Steven K. Mutschall^1 , Benjamin Hetman^2 , James E. Thomas^2 , Victor P. J. Gannon^1 , and Eduardo N. Taboada^1,2

> - ^1 Laboratory for Foodborne Zoonoses, Public Health Agency of Canada.

> - ^2 Department of Biological Sciences, University of Lethbridge.


Although molecular typing methods have traditionally been used for the characterization of bacterial pathogens, more recently, whole genome sequence (WGS) analysis has emerged as a tool that can be used to address population genetics for public health applications. WGS data can, in principle, resolve bacterial isolates that differ by a single base pair, thus providing the highest level of discriminatory power for epidemiological subtyping.

We present the Campylobacter *In Silico* Typing Server, a bioinformatics resource for rapidly performing a comprehensive suite of analyses from draft genome assemblies. In addition to performing pan-genome analyses that include core genome-based phylogenetic analysis and accessory genome content distribution analysis, this resource integrates the analysis of several DNA-based genotyping schemes that include flaA typing, porA typing, Multi-Locus Sequence Typing, Comparative Genomic Fingerprinting, and AMR profiling. 

In silico genotyping results provide a link between historical typing data and WGS data, while the whole genome phylogenetic analysis provides a framework for the comparison of molecular typing methodologies. In addition, this analysis allows for the identification of genotypic clusters and the exploration of associations between specific genotypes and phenotypic/epidemiologic metadata (e.g., geospatial distribution, host, source) for the identification of group-specific genomic markers (presence/absence of specific genomic regions, and single-nucleotide polymorphisms). 

This platform provides efficient algorithms and pre-computed analyses for the near real-time analyses of thousands of genomic sequences for population genomics, epidemiology, and clinical studies. Collaboration between similar international initiatives will allow for a world-wide real-time surveillance and analyses network for this important pathogen.

---

<img src="phac_logo.svg" alt="PHAC" id="phac_logo" style="height: 30px !important; padding-left: 20px;"/>
<img src="uleth_logo.jpeg" alt="Uleth" id="uleth_logo" style="height: 80px !important; padding-left: 20px;"/>
