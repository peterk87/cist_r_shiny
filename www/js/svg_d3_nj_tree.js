
<script src="js/d3.v3.min.js"></script>
<script src="js/colorbrewer.js"></script>
<script src="js/jquery-1.9.1.min.js"></script>
<script>
var svgOutputBinding = new Shiny.OutputBinding();
$.extend(svgOutputBinding, {
    find: function(scope) {
      return $(scope).find('.shiny-svg-output');
    },
    renderValue: function(el, data) {
      //use join() to combine the array of strings in data that comprise the svg code
      //data is sent from shiny
      $(el).html(data.join(''));
      var svg = d3.select(el).select("svg")
        .attr("pointer-events", "all");

      var g = svg.selectAll('g').filter(function(d, i) { return i === 1});
      // console.log(g);
      // svg.selectAll('g').filter(function(d, i) { return i === 1}).remove();
      // console.log('removed g');
      // console.log(g);
      // svg.append('g');
      
      g.call(d3.behavior.zoom().on("zoom", redraw));

      function redraw() {
        console.log("here", d3.event.translate, d3.event.scale);
        g.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")"); 
      } 
    }
  });
  Shiny.outputBindings.register(svgOutputBinding, 'cist.svgbinding');

</script>