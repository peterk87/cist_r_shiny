<style>

  #tooltip_mst {
  display:none;
  position:absolute;
  top:140px;
  left:900px;
  background-color:#444;
  color:#ddd;
  font-size:10pt;
  text-shadow: none;
  opacity:0.8;
  border-radius:10px;
  padding:5px;
  }

  .background {
  fill: #eee;
  }

  .title {
    font: 300 16px Helvetica Neue;
    fill: #666;
  }

  .node {
    stroke: #fff;
    stroke-width: 1.5px;
  }

  .link {
    stroke: #999;
    stroke-opacity: 0.6;
  }

  .link.active {
    stroke: red;
    stroke-width: 2px;
    stroke-opacity: 1;
  }

  .node.active {
    stroke: red;
    stroke-width: 3px;
  }

</style>

<script src="js/d3.v3.min.js"></script>
<script type="text/javascript">

  function handleMouseMove(event) {
    event = event || window.event; // IE-ism
    mousePos = {
      x: event.clientX,
      y: event.clientY
    };
  }
  window.onmousemove = handleMouseMove;

  function hideTooltip_mst() {
    $("#tooltip_mst")
    .text("")
    .css("display", "none");
  }

  var new_plot = true, 
      buckets = 256,
      n_spectral = 11,
      svg,
      rect,
      nodes,
      node,
      link,
      force;

  var colors = d3.scale.linear()
    .domain(d3.range(n_spectral).map(function(i){return i; }))
    .range(colorbrewer.Spectral[n_spectral])
    .interpolate(d3.interpolateHcl);

  var networkOutputBinding = new Shiny.OutputBinding();
  $.extend(networkOutputBinding, {
    find: function(scope) {
      return $(scope).find('.shiny-network-output');
    },
    renderValue: function(el, data) {
      console.log('renderValue');
      console.log(data);
      if (data == null) return;

      var max_distance = data.max_distance;

      console.log(max_distance);
      var colorScale = d3.scale.quantile()
        .domain([0, max_distance])
        .range(d3.range(buckets).map(function (i) {
          return colors(i/buckets*n_spectral);
        }));

      
      
      //format nodes object
      nodes = new Array();
      for (var i = 0; i < data.names.length; i++){
        nodes.push({"name": data.names[i], "group":data.groups[i]})
      }
      

      console.log(data.links);
      
      
      if (new_plot) {
        new_plot = false;
        //remove the old graph
        svg = d3.select(el).select("svg");
        svg.remove();
        
        $(el).html("");
        svg = d3.select(el)
          .append("svg");

        rect = svg.append('rect')
          .attr("pointer-events", "all")
          .style('fill', 'none');
        
        svg = svg.append('g');
        function initSizes() {
          console.log('initSizes');
          width = $(el).width();
          height = $(window).height()-140;
          rect.attr('height', height)
            .attr('width', width);
          force.size([width, height]);
          d3.select(el).select('svg')
            .attr("width", width)
            .attr("height", height);
        };
        
        initSizes();
        rect.call(d3.behavior.zoom().on('zoom', redraw));

        function redraw() {
          // console.log("here", d3.event.translate, d3.event.scale);
          svg.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")"); 
        } 
        force = d3.layout.force()
          .nodes(nodes)
          .links(data.links)
          .charge(-120)
          .linkDistance(function (d) { return Math.log(d.distances); })
          .linkStrength(function (d) { return 1 - (d.distances / data.max_distance); })
          .start();
        console.log(force.links());
        // svg.selectAll("line.link").remove();
        link = svg.selectAll("line.link")
            .data(force.links(), function(d) {
              return d.distances + ',' + d.source.name + ',' + d.target.name;
            });
        link.enter().append("line")
          .attr("class", "link")
          .style("stroke-width", function(d) { 
            var x = data.max_distance - d.distances;
            if (x === 0 || x === 1) {
              return 0.5;
            } else {
              return Math.log(x)/2.0;
            }
          })
          .style('stroke', function(d) { return colorScale(d.distances); })
          .on("mouseover", linkMouseover)
          .on("mouseout", mouseout);
        link.exit().remove();
        force.on("tick", function() {
          link.attr("x1", function(d) { return d.source.x; })
              .attr("y1", function(d) { return d.source.y; })
              .attr("x2", function(d) { return d.target.x; })
              .attr("y2", function(d) { return d.target.y; });
      
          node.attr("cx", function(d) { return d.x; })
              .attr("cy", function(d) { return d.y; })
        });
      }



      node = svg.selectAll("circle.node")
        .data(nodes, function(d) { return d.name; });
      node.enter().append("circle")
        .attr("class", "node")
        .attr("r", function(d) {return 10;})
        .call(force.drag)
        .on("mouseover", nodeMouseover)
        .on("mouseout", mouseout)
        .on("click", node_mouse_click);
      node.style("fill", function(d) { return d.group; });
      node.exit().remove();
        
      

      function showTooltip(e, html) {
        var tt = $("#tooltip_mst"), x = (mousePos.x + 10), y = (mousePos.y + 10);
        tt.html(html);
        tt.css("left", x + "px")
        .css("top", y + "px")
        .css("display", "block");
        if (mouseover_show_table) {
          tt.css('border', '2px solid black');
        } else {
          tt.css('border', '2px solid white');
        }
      }

      mouseover_show_table = true;

      // Highlight the link and connected nodes on mouseover.
      function linkMouseover(d) {
        svg.selectAll("line.link").classed("active", function(p) { return p === d; });
        svg.selectAll("node").classed("active", function(p) {
         return p === d.source || p === d.target; 
        });
      }
      
      // Highlight the node and connected links on mouseover.
      function nodeMouseover(d) {
        svg.selectAll("line.link").classed("active", function(p) { return p.source === d || p.target === d; });
        d3.select(this).classed("active", true);

        // console.log(d);
        if (mouseover_show_table){
          generate_node_table(d);
        }
      }
      
      function node_mouse_click(p) {
        mouseover_show_table = !mouseover_show_table;
        generate_node_table(p);
      }

      function generate_node_table(d){
        // console.log('generate_node_table::');
        var forcelinks = force.links();
        // console.log(forcelinks);
        var linkNames = {};
        for (var i = 0; i < forcelinks.length; i++){
          if (forcelinks[i].source === d){
            linkNames[forcelinks[i].target.index] = forcelinks[i].distances;
          }
          if (forcelinks[i].target === d){
            linkNames[forcelinks[i].source.index] = forcelinks[i].distances;
          }
        }
        
        delete linkNames[d];

        // console.log(linkNames);
        
        md = "<table>";
        md += '<tr><th><input type="button" class="btn" name="cancelvalue" value="Close" onClick="self.hideTooltip_mst();self.mouseover_show_table = false;"></th><th>Dist. from ' + d.name + '</th>';
        for (var key in data.md)
        {
          md += '<th>' + key + '</th>';
        }
        md += '</tr>';
        md += '<tr><td><b>' + d.name + '</b></td><td></td>';
        for (var md_key in data.md)
        {
          var style = '';
          if (data.selected_md == md_key)
          {
            var background_color = data.groups[d.index];
            var d3_hcl = d3.hcl(background_color);
            var font_color = 'white';
            if (d3_hcl.l > 20) 
            {
              font_color = 'black';
            }
            style = 'style="background-color:' + background_color + '; color:' + font_color + ';"';
          }
          md += '<td ' + style + '>' + data.md[md_key][d.index] + '</td>';
        }
        md += '</tr>';
        for (var key in linkNames) 
        {
          md += '<tr><td><b>' + data.names[key] + '</b></td><td style="background-color:' + colorScale(linkNames[key]) + '; color:black;">' + linkNames[key] + '</td>';
          for (var md_key in data.md)
          {
            var style = '';
            if (data.selected_md == md_key)
            {
              var background_color = data.groups[key];
              var d3_hcl = d3.hcl(background_color);
              var font_color = 'white';
              if (d3_hcl.l > 20) 
              {
                font_color = 'black';
              }
              style = 'style="background-color:' + background_color + '; color:' + font_color + ';"';
            }
            md += '<td ' + style + '>' + data.md[md_key][key] + '</td>';
          }
          md += '</tr>';
        };
        md += "</table>";

        // console.log(md);
        showTooltip(d, md);
        // console.log(d);
      }

      // Clear any highlighted nodes or links.
      function mouseout() {
        svg.selectAll(".active").classed("active", false);
      }

      $(window).resize(initSizes);
    }
  });
  Shiny.outputBindings.register(networkOutputBinding, 'cist.networkbinding');
  
  </script>
