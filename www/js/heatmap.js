<style>

	#tooltip_hm {
	display:none;
	position:absolute;
	top:195px;
	left:1155px;
	/*width:200px;*/
	background-color:#444;
	color:#ddd;
	font-size:10pt;
	text-shadow: none;
	opacity:0.8;
	border-radius:10px;
	padding:5px;
	}

	.background {
	fill: #eee;
	}

	line {
	stroke: #fff;
	}
	text {
	fill: #000;
	}
	text.active {
	fill: #14C412;
	}

</style>

<script src="js/d3.v3.min.js"></script>
<script src="js/colorbrewer.js"></script>

<script>
  function hideTooltip_hm() {
    $("#tooltip_hm")
    .text("")
    .css("display", "none");
  }
  var heatmapOutputBinding = new Shiny.OutputBinding();
	$.extend(heatmapOutputBinding, {
  	find: function(scope) {
  		return $(scope).find('.shiny-heatmap-output');
  	},
  	renderValue: function(el, data) {
      console.log(data);
  		var margin = {top: 90, right: 20, bottom: 40, left: 90},
    		width = $(window).height()*.7-140,
    		height = $(window).height()*.7-140;
      console.log(margin);
  		var x = d3.scale.ordinal().rangeBands([0, width]),
    		z = d3.scale.linear().domain([0, 4]).clamp(true),
    		c = d3.scale.category10().domain(d3.range(10));
      console.log('setup x, z, c');
      //remove the old graph
      var svg = d3.select(el).select("svg");      
      svg.remove();
      console.log('removed old svg');
      $(el).html("");
      console.log('el html ""');
  		var svg = d3.select(el)
        .append("svg")
        .attr("pointer-events", "all")
        .append("g")
        .call(d3.behavior.zoom().on("zoom", redraw))
        .append('svg:g')
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

      // make a huge background rect so that user can click and drag the svg by
      // the background - very hackish
      svg.append('svg:rect')
        .attr('width', 999999)
        .attr('height', 999999)
        .attr('x', -999999/2)
        .attr('y', -999999/2)
        .attr('fill', '#eee');

      function initSizes() {
        console.log('initSizes');
        width = $(el).width();
        height = $(window).height()-140;
        console.log([width, height]);
        console.log(svg);
        d3.select(el).select('svg')
          .attr("width", width)
          .attr("height", height);
      };
      
      initSizes();

      function redraw() {
        console.log("here", d3.event.translate, d3.event.scale);
        svg.attr("transform","translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")"); 
      } 

      console.log('svg setup');
  		var buckets = 256;
  		var n_spectral = 11;
  		var colors = d3.scale.linear()
    		.domain(d3.range(n_spectral).map(function(i){return i; }))
    		.range(colorbrewer.Spectral[n_spectral])
    		.interpolate(d3.interpolateHcl);
      console.log('colors setup');

      var max_distance = data.max_distance;

      console.log(max_distance);
  		var colorScale = d3.scale.quantile()
    		.domain([0, max_distance])
    		.range(d3.range(buckets).map(function (i) {
    		  return colors(i/buckets*n_spectral);
    		}));
      console.log('color scale setup');
      console.log(data.strain);
      console.log(data.strain.length);
  		var matrix = [],
  		  n = data.strain.length;
      console.log(n);
  		// Compute index per node.
  		data.strain.forEach(function(strain, i) {
  		    matrix[i] = d3.range(n).map(function(j) { return {x: j, y: i, z: data.distances[(i) + (j * n) ]}; });
  	  });

  		// Precompute the orders.
  		var orders = {
  		  name: d3.range(n).sort(function(a, b) { return d3.ascending(data.strain[a], data.strain[b]); }),
  		  hc: d3.range(n)
  		};
  		console.log(orders);
  		// The default sort order.
  		x.domain(orders.name);


  		var row = svg.selectAll(".row")
    		.data(matrix)
    		.enter().append("g")
    		.attr("class", "row")
    		.attr("transform", function(d, i) { return "translate(0," + x(i) + ")"; })
    		.each(row);


  		row.append("text")
    		.attr("x", -6)
    		.attr("y", x.rangeBand() / 2)
    		.attr("dy", ".32em")
    		.attr("text-anchor", "end")
        .attr("font-size", (x.rangeBand() > 10) ? 10 : x.rangeBand())
    		.text(function(d, i) { return data.strain[i]; });

  		var column = svg.selectAll(".column")
    		.data(matrix)
    		.enter().append("g")
    		.attr("class", "column")
    		.attr("transform", function(d, i) { return "translate(" + x(i) + ")rotate(-90)"; });


  		column.append("text")
    		.attr("x", 6)
    		.attr("y", x.rangeBand() / 2)
    		.attr("dy", ".32em")
    		.attr("text-anchor", "start")
        .attr("font-size", (x.rangeBand() > 10) ? 10 : x.rangeBand())
    		.text(function(d, i) { return data.strain[i]; });

  		function row(row) {
    		var cell = d3.select(this).selectAll(".cell")
      		.data(row)
      		.enter().append("rect")
      		.attr("class", "cell")
      		.attr("x", function(d) { return x(d.x); })
      		.attr("width", x.rangeBand())
      		.attr("height", x.rangeBand())
      		.style("fill", function(d) { return colorScale(d.z); })
      		.on("mouseover", mouseover)
      		.on("mouseout", mouseout)
      		.on("click", mouse_click);
  		}

      // Legend
  		var legendElementWidth = ($(window).height()*.7-140)/buckets;
  		var colorScaleQuantiles = [0].concat(colorScale.quantiles());
  		
  		
      var legend = svg.selectAll(".legend")
    		.data(colorScaleQuantiles)
    		.enter().append("g")
    		.attr("class", "legend");
      console.log('legend:')
  		console.log(legendElementWidth);
  		console.log(legendElementWidth * buckets);
  		legend.append("rect")
    		.attr("x", function(d, i) { return legendElementWidth * i; })
    		.attr("y", ($(window).height()*.7-140) + 5)
    		.attr("width", legendElementWidth)
    		.attr("height", 20)
    		.style("fill", function(d, i) { return colorScale(d); });


      // only show 10 legend labels at the appropriate increments
  		var legend_label_incr = Math.floor(buckets/10);

  		legend.filter(function (d, i) { return (i % legend_label_incr) === 0; })
    		.append("rect")
    		.attr("x", function(d, i) { return legendElementWidth * i * legend_label_incr; })
    		.attr("y", ($(window).height()*.7-140) + 25)
    		.attr("width", legendElementWidth)
    		.attr("height", 3)
    		.style("fill", "black");

  		legend.filter(function (d, i) { return (i % legend_label_incr) === 0; })
    		.append("text")
    		.attr("class", "mono")
    		.text(function(d) { return d.toFixed(0); })
    		.attr("x", function(d, i) {return legendElementWidth * i * legend_label_incr; })
    		.attr("y", ($(window).height()*.7-140) + 39)
    		.attr("text-anchor", "middle");

  		function showTooltip(e, html) {
    		var tt = $("#tooltip_hm"), x = (e.pageX + 10), y = (e.pageY + 10);
    		tt.html(html);
        console.log('showTooltip');
        console.log(tt);
    		console.log(e);
        console.log($(window).height());
        console.log($(window).width());
    		if (y -10 + tt.height() > $(window).height()) {
    		y = $(window).height() - tt.height() - 20;
    		}
    		if (x -10 + tt.width() > $(window).width()) {
    		x = $(window).width() - tt.width() - 20;
    		}
        console.log('x: ' + x);
        console.log('y: ' + y);
    		tt.css("left", x + "px")
    		.css("top", y + "px")
    		.css("display", "block");
        if (mouseover_show_table){
          tt.css('border', '2px solid black');
        }
        else {
          tt.css('border', '2px solid white');
        }

  		}
      mouseover_show_table = true;



      function mouseover(p) {
        d3.selectAll(".row text").classed("active", function(d, i) { return i == p.y; });
        d3.selectAll(".column text").classed("active", function(d, i) { return i == p.x; });
        console.log(p);
        if (mouseover_show_table){
          generate_show_md_table(p);
        }
      }

  		function mouse_click(p) {
        mouseover_show_table = !mouseover_show_table;
        generate_show_md_table(p);
  		}

      function generate_show_md_table(p){
        md = "<table>";
        md += '<tr><th><input type="button" class="btn" name="cancelvalue" value="Close" onClick="self.hideTooltip_hm();self.mouseover_show_table = false;"></th><th>' + data.strain[p.x] + '</th><th>' + data.strain[p.y] + '</th></tr>'
        for (var key in data.md) {
          if (key !== 'distances'){
            md += '<tr><td><b>' + key + '</b></td><td>' + data.md[key][p.x] + '</td><td>' + data.md[key][p.y] + '</td></tr>';
          }
        };
        md += '<tr><td><b>Distance</b></td><td>' + p.z + '</td><td style="background-color:' + colorScale(p.z) + ';"></td></tr>'; 
        md += "</table>";
        console.log(md);
        showTooltip(p, md);
        console.log(p);
      }

  		function mouseout() {
  		  d3.selectAll("text").classed("active", false);
  		}

  		d3.select("#order").on("change", function() {
    		clearTimeout(timeout);
    		order(this.value);
  		});

  		function order(value) {
    		x.domain(orders[value]);

    		var t = svg.transition().duration(500);

    		t.selectAll(".row")
      		.attr("transform", function(d, i) { return "translate(0," + x(i) + ")"; })
      		.selectAll(".cell")
      		.attr("x", function(d) { return x(d.x); });

    		t.selectAll(".column")
      		.attr("transform", function(d, i) { return "translate(" + x(i) + ")rotate(-90)"; });
  		}

  		var timeout = setTimeout(function() {
  		order("hc");
  		d3.select("#order").property("selectedIndex", 1).node().focus();
  		}, 2000);

      function onResize() {
        initSizes();
      };
      $(window).resize(onResize);
    }
	});
	Shiny.outputBindings.register(heatmapOutputBinding);

  // var heatmapInputBinding = new Shiny.InputBinding();
  // $.extend(heatmapInputBinding, {
  //   find: function(scope) {
  //     return $(scope).find('.shiny-heatmap-output');
  //   },
  //   getValue: function(el) {
  //     var svg = d3.select(el).select("svg");
  //     return svg;
  //   },
  //   subscribe: function(el, callback) {
  //     $(el).on("change.heatmapInputBinding", function(e) {
  //       callback();
  //     });
  //   }
  // });
  // Shiny.inputBindings.register(heatmapInputBinding);
</script>