<script type="text/javascript" src="js/intro.js"></script>
<script type="text/javascript">
  
  function start_intro() {
    active_tab = document.querySelector('.active').children[0]
    tab_intro_dict = {
      'Home': start_home_intro,
      'Metadata Distribution': start_metadata_distribution_intro,
      'NJ Tree': start_nj_tree_intro,
      'MST': start_mst_intro,
      'Heatmap': start_heatmap_intro,
      'D3Heatmap': start_d3_heatmap_intro,
      'Partition Congruence': start_partition_congruence_intro,
      'View Metadata': start_view_metadata_intro
    }
    tab_intro_dict[active_tab.text]()
  }

  function start_metadata_distribution_intro(){
    var intro = introJs();
    intro.setOptions({
      steps: [
        {
          // element: document.querySelector('#visualize_distr'),
          element: '#visualize_distr',
          intro: "This is a plot of the frequency of the selected types for the selected metadata category. The types are plotted in order of most to least frequent. You can also select what other metadata category to highlight.",
          position: 'left'
        },
        {
          element: '#crosstab_explanation',
          intro: "Below is a cross tabulation of the selected types within the selected category or categories. This data is plotted above in the bar graph.",
          position: 'top'
        },
        {
          element: '#category1',
          intro: "Select the category to plot the frequency distribution for.",
          position: 'right'
        },
        {
          element: '#categoryGroups1',
          intro: 'Select the types within the selected category to specifically plot.',
          position: 'right'
        },
        {
          element: '#category2',
          intro: "Select another category to highlight in the plot.",
          position: 'right'
        },
        {
          element: '#categoryGroups2',
          intro: 'Select the groups within the above category to highlight.',
          position: 'right'
        }
      ]
    });

    intro.start();
  }
  function start_nj_tree_intro (){
    var intro = introJs();
    intro.setOptions({
      steps: [
        {
          // element: document.querySelector('#visualize_distr'),
          element: '#nj_tree_plot',
          intro: "This is a plot of a neighbour-joining (NJ) tree using the underlying pairwise SNP distance for the selected strains. You can select what metadata to visualize in the sidebar panel.",
          position: 'left'
        },
        {
          element: document.querySelector('#category1').parentElement,
          intro: "Select the category to select groups of strains to plot an NJ tree for.",
          position: 'right'
        },
        {
          element: document.querySelector('#categoryGroups1').parentElement,
          intro: 'Select the groups within the selected category to plot.',
          position: 'right'
        },
        {
          element: document.querySelector('#category2').parentElement,
          intro: "Select another category to highlight in the plot.",
          position: 'right'
        },
        {
          element: document.querySelector('#categoryGroups2').parentElement,
          intro: 'Select the groups within the above category to highlight.',
          position: 'right'
        },
        {
          element: document.querySelector('#label_category').parentElement,
          intro: 'Select the metadata category to use for the tip labels in the tree.',
          position: 'right'
        },
        {
          element: document.querySelector('#show_missing_labels').parentElement,
          intro: 'Check/uncheck to show/hide labels which contain missing data.',
          position: 'right'
        },
        {
          element: document.querySelector('#show_scale_bar').parentElement,
          intro: 'Show or hide the scale bar for the NJ tree.',
          position: 'right'
        },
        {
          element: document.querySelector('#label_size').parentElement,
          intro: 'Adjust the size of the tip labels in the tree',
          position: 'right'
        },
        {
          element: document.querySelector('#tree_type').parentElement,
          intro: 'Change the type of phylogenetic tree to plot',
          position: 'right'
        },
        {
          element: document.querySelector('#save_nj_tree_plot').parentElement,
          intro: 'Save a copy of the tree as a PDF. Adjust the size of the plot height and width using the above text boxes.',
          position: 'right'
        }
      ]
    });

    intro.start();
  }
  function start_mst_intro () {
    var intro = introJs();

    intro_steps = [
      {
        // element: document.querySelector('#visualize_distr'),
        element: '#mst_d3',
        intro: '<p>This is an interactive minimum spanning tree (MST) using the *igraph* R package and the <a href="http://d3js.org/" target="D3">D3 JavaScript library</a>.</p><ul><li><em>Hover</em> over nodes to view the underlying metadata for the parent and children node strains </li><li><em>Click</em> on a node to lock or unlock the parent and children node strains metadata table</li><li>Zoom with the <em>mousewheel</em> </li><li>Pan by <em>clicking and dragging</em> the MST around</li></ul><b>Larger numbers of selected strains may crash your browser due to the memory and CPU requirements.</b>',
        position: 'left'
      },
      {
        element: '#tooltip',
        intro: "This is a table of metadata for a strain to strain comparison you have highlighted.",
        position: 'left'
      },
      {
        element: document.querySelector('#category1').parentElement,
        intro: "Select the category to select groups of strains to plot an NJ tree for.",
        position: 'right'
      },
      {
        element: document.querySelector('#categoryGroups1').parentElement,
        intro: 'Select the groups within the selected category to plot.',
        position: 'right'
      },
      {
        element: document.querySelector('#category2').parentElement,
        intro: "Select another category to highlight in the plot.",
        position: 'right'
      },
      {
        element: document.querySelector('#categoryGroups2').parentElement,
        intro: 'Select the groups within the above category to highlight.',
        position: 'right'
      }
    ];
    
    console.log(intro_steps[1]);
    if (document.querySelector('#tooltip').style.display != 'block') {
      intro_steps.splice(1, 1);
      console.log(intro_steps);
    }
    intro.setOptions({
      steps: intro_steps
    });
    intro.start();
  }
  function start_heatmap_intro(){
    var intro = introJs();
      intro.setOptions({
        steps: [
          {
            element: '#heatmap_plot',
            intro: "This is a hierarchically clustered heatmap plot of genetic distance between all strains in the selected types within a selected category. ",
            position: 'left'
          },
          // {
          //   element: '#crosstab_explanation',
          //   intro: "Below is a cross tabulation of the selected types within the selected category or categories. This data is plotted above in the bar graph.",
          //   position: 'top'
          // },
          {
            element: document.querySelector('#category1').parentElement,
            intro: "Select the category to plot the frequency distribution for.",
            position: 'right'
          },
          {
            element: document.querySelector('#categoryGroups1').parentElement,
            intro: 'Select the types within the selected category to specifically plot.',
            position: 'right'
          },
          {
            element: document.querySelector('#category2').parentElement,
            intro: "Select another category to highlight in the plot.",
            position: 'right'
          },
          {
            element: document.querySelector('#categoryGroups2').parentElement,
            intro: 'Select the groups within the above category to highlight.',
            position: 'right'
          }
        ]
      });

      intro.start();
  }
  function start_home_intro(){
    var intro = introJs();
    intro_steps = [
        {
          element: '#datatabs',
          intro: "Click on one of these tabs to use an analysis tool, view metadata, or visualize the data within this dataset.",
          position: 'bottom'
        },
        {
          // home tab
          element: document.querySelector('#datatabs').children[0],
          intro: "This is the tab you are currently in. It contains some information on this analysis tool as well as links of interest to related projects and publications.",
          position: 'bottom'
        },
        {
          // metadata distribution tab
          element: document.querySelector('#datatabs').children[1],
          intro: 'View the distribution of different types of metadata within the dataset.',
          position: 'bottom'
        },
        {
          element: document.querySelector('#datatabs').children[2],
          intro: 'View a phylogenetic tree with metadata highlighted.',
          position: 'bottom'
        },
        {
          // MST tab
          element: document.querySelector('#datatabs').children[3],
          intro: "View an interactive minimum spanning tree (MST) with metadata highlighted.",
          position: 'bottom'
        },
        {
          // heatmap tab
          element: document.querySelector('#datatabs').children[4],
          intro: "View a hierarchically clustered heatmap of pairwise genetic distances.",
          position: 'bottom'
        },
        {
          // D3 heatmap tab
          element: document.querySelector('#datatabs').children[5],
          intro: 'View an interactive heatmap for a subset of the dataset.',
          position: 'bottom'
        },
        {
          // partition congruence tab
          element: document.querySelector('#datatabs').children[6],
          intro: 'Quantitatively compare methods against other methods as well as against whole-genome sequence analysis derived phylogenomic clusters.',
          position: 'bottom'
        },
        {
          // view metadata tab
          element: document.querySelector('#datatabs').children[7],
          intro: 'View and explore the metadata for this dataset.',
          position: 'bottom'
        }
      ];
    intro.setOptions({
      steps: intro_steps
    });

    intro.start();
  }
  function start_d3_heatmap_intro(){
    var intro = introJs();
    intro_steps = [
      {
        element: document.querySelector('#jsheatmap'),//.children[0],
        intro: '<p>This is an interactive heatmap using the <a href="http://d3js.org/" target="D3">D3 JavaScript library</a>.</p><ul><li><em>Hover</em> over cells to view the underlying metadata for the strain to strain comparison </li><li><em>Click</em> on a cell to lock or unlock the strain to strain comparison data table</li><li>Zoom with the <em>mousewheel</em> </li><li>Pan by <em>clicking and dragging</em> the heatmap around</li></ul><b>This visualization is currently limited to matrices with a maximum size of 300x300. Larger matrices may crash your browser due to the memory and CPU requirements.</b>',
        position: 'left'
      },
      {
        element: '#tooltip',
        intro: "This is a table of metadata for a strain to strain comparison you have highlighted.",
        position: 'left'
      },
      {
        element: document.querySelector('#category1'),
        intro: "Select the category to generate a heatmap for. <b>Currently only heatmaps of 300 strains can be generated due to memory and CPU limitations (up to ~100 strains recommended).</b>",
        position: 'right'
      },
      {
        element: document.querySelector('#categoryGroups1'),
        intro: 'Select the types within the selected category to specifically plot.',
        position: 'right'
      },
      {
        element: document.querySelector('#category2'),
        intro: "Select another category to highlight in the plot.",
        position: 'right'
      },
      {
        element: document.querySelector('#categoryGroups2'),
        intro: 'Select the groups within the above category to highlight.',
        position: 'right'
      },
      {
        element: document.querySelector('#order'),
        intro: 'Choose how to order the strains in the heatmap.',
        position: 'right'
      }
    ];
    console.log(intro_steps[1]);
    if (document.querySelector('#tooltip').style.display != 'block') {
      intro_steps.splice(1, 1);
      console.log(intro_steps);
    }

    intro.setOptions({steps: intro_steps});
    intro.start();
  }
  function start_partition_congruence_intro(){
    var intro = introJs();
    intro.setOptions({
      steps: [
        {
          element: '#clusters_A_ui',
          intro: "Select one or more methods to show partition congruence metrics for",
          position: 'right'
        },
        {
          element: '#clusters_B_ui',
          intro: "Select one or more methods to show partition congruence metrics for",
          position: 'right'
        },
        {
          element: '#partition_congruence_metrics_ui',
          intro: "Select partition congruence metrics to show",
          position: 'right'
        },
        {
          element: '#dl_partition_congruence_table',
          intro: 'Download the table of partition congruence metrics',
          position: 'right'
        }
      ]
    });

    intro.start();
  }
  function start_view_metadata_intro(){
    var intro = introJs();
    intro.setOptions({
      steps: [
        {
          element: '#columns',
          intro: "Select which columns to show in the table view.",
          position: 'right'
        },
        {
          element: '#nrRows',
          intro: "Adjust the slide to browse through the table view.",
          position: 'right'
        },
        {
          element: '#md_view_filter_columns',
          intro: "Use regular expressions to select which column(s) to apply item filter in. <i>Some useful regular expression tips and examples are shown below under '<b>How to use regular expressions</b>'</i>",
          position: 'right'
        },
        {
          element: '#md_view_filter_items',
          intro: 'Use regular expressions to select which item(s) to filter for in the specified column(s)',
          position: 'right'
        },
        {
          element: '#dl_metadata_subset',
          intro: "Download a metadata table corresponding to the subset of strains you have selected in your analysis (or selected using regular expressions).",
          position: 'right'
        }
      ]
    });

    intro.start();
  }
</script>