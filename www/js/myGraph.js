// 


function hideTooltip_mst() {
  $('#tooltip_mst')
  .text('')
  .css('display', 'none');
}
var mousePos, mouseover_show_table = true;
function myGraph(el) {

  function handleMouseMove(event) {
    event = event || window.event; // IE-ism
    mousePos = {
      x: event.clientX,
      y: event.clientY
    };
  }
  window.onmousemove = handleMouseMove;


  // Add and remove elements on the graph object
  this.addNode = function (id, colour) {
    var nodeMatch = findNode(id);
    if (typeof(nodeMatch) === 'undefined') {
      nodes.push({'id':id, 'colour':colour, 'value':-1});
    } else {
      nodeMatch.colour = colour;
    }
  };

  this.addNodes = function (ids, colours) {
    var d_n = {};
    for (var i = 0; i < ids.length; i++) {
      var id = ids[i];
      d_n[id] = true;
      this.addNode(id, colours[i]);
    }
    var n = this.getNodes();
    for (i = n.length - 1; i >= 0; i--) {
      var ni = n[i];
      var n_key = ni.id;
      if (!(n_key in d_n)) {
        this.removeNode(n_key);
      }
    }
  };

  this.getNodes = function () {
    return nodes;
  };

  this.removeNode = function (id) {
    var i = 0;
    var n = findNode(id);
    while (i < links.length) {
      if ((links[i].source == n)||(links[i].target == n))
      {
        links.splice(i,1);
      }
      else i++;
    }
    nodes.splice(findNodeIndex(id),1);
  };

  this.removeLink = function (source,target){
    for(var i=0;i<links.length;i++)
    {
      if(links[i].source.id == source && links[i].target.id == target)
      {
        links.splice(i,1);
        break;
      }
    }
  };

  this.removeallLinks = function(){
    links.splice(0,links.length);
  };

  this.removeAllNodes = function(){
    nodes.splice(0,links.length);
  };

  this.addLink = function (source, target, value) {
    links.push({'source':findNode(source),'target':findNode(target),'value':value});
  };

  this.addLinks = function (newLinks) {
    var d_l = {};
    for (var i = 0; i < newLinks.length; i++) {
      var x = newLinks[i];
      if (!(x.source + '-' + x.target in d_l || x.target + '-' + x.source in d_l)) {
        d_l[x.source + '-' + x.target] = true;
        this.addLink(x.source, x.target, x.value);
      }
    }
    var l = this.getLinks();
    for (i = l.length - 1; i >= 0; i--) {
      var li = l[i];
      var l_key = li.source.id + '-' + li.target.id;
      var l_key2 = li.target.id + '-' + li.source.id;
      if (!(l_key in d_l || l_key2 in d_l)) {
        this.removeLink(li.source.id, li.target.id);
      }
    }
  };

  this.getLinks = function() {
    return links;
  };

  var findNode = function(id) {
    for (var i in nodes) {
      if (nodes[i].id === id) {
        return nodes[i];
      }
    }
  };

  var findNodeIndex = function(id) {
    for (var i=0;i<nodes.length;i++) {
      if (nodes[i].id==id){
        return i;
      }
    }
  };
  // set up the D3 visualisation in the specified element
  var w = 800,
  h = 800;
  var vis = d3.select(el)
    .append('svg')
    .attr('id','svg')
    .attr('pointer-events', 'all');

  var rect = d3.select(el).select('svg')
    .append('rect')
    .attr('pointer-events', 'all')
    .style('fill', 'none');

  rect.call(d3.behavior.zoom().on('zoom', redraw));

  vis = vis.append('g');

  function redraw() {
    vis.attr('transform','translate(' + d3.event.translate + ')' + ' scale(' + d3.event.scale + ')');
  }

  function initSizes() {
    w = $(el).width();
    h = $(window).height()-140;
    force.size([w, h]);
    d3.select(el).select('svg')
      .attr('width', w)
      .attr('height', h);
    rect
      .attr('height', h)
      .attr('width', w);
  }

  var force = d3.layout.force();

  var nodes = force.nodes(),
  links = force.links();
  initSizes();
  var data, colorScale;
  var colors = d3.scale.linear()
    .domain(d3.range(11).map(function(i){return i; }))
    .range(colorbrewer.Spectral[11])
    .interpolate(d3.interpolateHcl);
  this.update = function (max_distance, show_distances, dat) {
    data = dat;

    colorScale = d3.scale.quantile()
      .domain([0, max_distance])
      .range(d3.range(256).map(function (i) { return colors(i/256*11); }));

    var link = vis.selectAll('g.link')
      .data(links, function(d) {
        return d.source.id + '-' + d.target.id;
      });

    var linkEnter = link.enter().append('g')
      .attr('id',function(d){return d.source.id + '-' + d.target.id;})
      .attr('class','link')
      .classed('derp', true);

    linkEnter.append('line')
      .style('stroke', 'black')
      .style('stroke-width', function(d) {
        return (5*(1-(d.value / max_distance))) + 'px';
      });
    linkEnter.append('text')
      .attr('class','distance-label')
      .attr('dy', '.71em')
      .attr('text-anchor', 'middle')
      .attr('font-size', '20px')
      .attr('font-family', 'sans-serif')
      .style('fill', 'black')
      .style('stroke', 'white')
      .style('opacity',0)
      .text(function(d){
        return d.value;
      });
    if (show_distances) {
      link.selectAll('.distance-label')
        .transition()
        .duration(500)
        .style('opacity',1)
        .text(function(d){
          return d.value;
        });
    } else {
      link.selectAll('.distance-label')
        .transition()
        .duration(500)
        .style('opacity',0);
    }
    link.exit().transition().duration(500).style('opacity', 0).remove();
    

    var node = vis.selectAll('.node')
    .data(nodes, function(d) {
      return d.id;});

    node.enter().append('circle')
      .attr('class', 'node')
      .attr('r', (typeof dat === 'undefined') ? 10 : dat.node_size)
      .call(force.drag)
      .on('mouseover', nodeMouseover)
      .on('mouseout', mouseout)
      .on('click', node_mouse_click);

    node.transition()
      .duration(100)
      .delay(function(d, i) { return i * 1; })
      .style('fill', function(d) { return d.colour; })
      .attr('r', (typeof dat === 'undefined') ? 10 : dat.node_size);

    node.classed('derp', true);
    node.exit().transition().duration(500).style('opacity', 0).remove();

    force.on('tick', function() {
      link.selectAll('text')
        .attr('x', function(d) { return (d.source.x + d.target.x)/2.0; })
        .attr('y', function(d) { return (d.source.y + d.target.y)/2.0; });
      link.selectAll('line')
        .attr('x1', function(d) { return d.source.x; })
        .attr('y1', function(d) { return d.source.y; })
        .attr('x2', function(d) { return d.target.x; })
        .attr('y2', function(d) { return d.target.y; });
      node.attr('transform', function(d) { return 'translate(' + d.x + ',' + d.y + ')'; });
    });

    // Restart the force layout.
    if (typeof dat === 'undefined') {
      force
        .gravity(0.05)
        .distance(50)
        .charge(-120)
        .size([w, h])
        .start();
    } else {
      force
        .gravity(dat.gravity)
        .distance(dat.distance)
        .charge(dat.charge)
        .size([w, h])
        .start();
    }

    d3.select(el).select('svg').selectAll('.derp').sort(function(a,b) {
      return d3.descending(a.value, b.value);
    });

    if (typeof dat === 'undefined' || dat.groups === null || dat.groups.length === 0) {
      // remove legend if there are no groups highlighted
      d3.select(el).select('svg').selectAll('.legend')
        .transition()
        .duration(500)
        .style('fill-opacity', 1e-6)
        .remove();
    } else {
      // update legend with new group data and colours
      var legend_data = [];
      // check if single group specified
      if (typeof dat.groups == 'string') {
        legend_data.push({group:dat.groups,
          group_colour:dat.group_colours[0]});
      } else {
        // if more than one group is highlighted
        for (var i = 0; i < dat.groups.length; i++) {
          legend_data.push({group:dat.groups[i],
            group_colour:dat.group_colours[i]});
        }
      }
      // remove the old legend
      d3.select(el).select('svg').selectAll('.legend').remove();
      // create new legend
      var legend = d3.select(el).select('svg').selectAll('.legend')
        .data(legend_data, function(d) { return d.group; })
        .enter()
        .append('g')
        .attr('class', 'legend')
        .attr('transform', function(d, i) { return 'translate(0,' + i * 20 + ')'; });
      // create rect elements with group colours
      legend.append('rect')
        .attr('x', w - 18)
        .attr('width', 18)
        .attr('height', 18)
        .style('fill', function(d) { return d.group_colour; });
      // create text elements with group names
      legend.append('text')
        .attr('x', w - 24)
        .attr('y', 9)
        .attr('dy', '.35em')
        .style('text-anchor', 'end')
        .text(function(d) { return d.group; });
    }
  };


  function showTooltip(e, html) {
    var tt = $('#tooltip_mst'), x = (mousePos.x + 10), y = (mousePos.y + 10);
    tt.html(html);
    tt.css('left', x + 'px')
      .css('top', y + 'px')
      .css('display', 'block');
    if (mouseover_show_table) {
      tt.css('border', '2px solid black');
    } else {
      tt.css('border', '2px solid white');
    }
  }

  

  // Highlight the link and connected nodes on mouseover.
  function linkMouseover(d) {
    vis.selectAll('g.link').classed('active', function(p) { return p === d; });
    vis.selectAll('node').classed('active', function(p) {
      return p === d.source || p === d.target;
    });
  }

  // Highlight the node and connected links on mouseover.
  function nodeMouseover(d) {
    vis.selectAll('g.link').classed('active', function(p) { return p.source === d || p.target === d; });
    d3.select(this).classed('active', true);

    // console.log(d);
    if (mouseover_show_table){
      generate_node_table(d);
    }
  }

  function node_mouse_click(p) {
    mouseover_show_table = !mouseover_show_table;
    generate_node_table(p);
  }

  function generate_node_table(d){
    var key, style, md_key, background_color, d3_hcl, font_color;
    var forcelinks = force.links();
    var linkNames = {};
    for (var i in forcelinks) {
      var x = forcelinks[i];
      if (x.source.id === d.id) {
        linkNames[x.target.id] = x.value;
      }
      if (x.target.id === d.id) {
        linkNames[x.source.id] = x.value;
      }
    }
    
    var html = '<table class="table-bordered">';
    html += '<tr><th><input type="button" class="btn btn-danger" name="cancelvalue" value="X" onClick="self.hideTooltip_mst();mouseover_show_table = false;"></th><th>Dist. from ' + d.id + '</th>';

    for (key in data.md) {
      html += '<th>' + key + '</th>';
    }
    html += '</tr>';
    html += '<tr align="right"><td><b>' + d.id + '</b></td><td></td>';
    var strain_idx = data.ids.indexOf(d.id);
    for (md_key in data.md) {
      style = '';
      if (data.selected_md == md_key) {
        background_color = data.colours[strain_idx];
        d3_hcl = d3.hcl(background_color);
        font_color = 'white';
        if (d3_hcl.l > 20) {
          font_color = 'black';
        }
        style = 'style="background-color:' + background_color + '; color:' + font_color + ';"';
      }
      html += '<td ' + style + '>' + data.md[md_key][strain_idx] + '</td>';
    }
    html += '</tr>';
    for (key in linkNames) {
      strain_idx = data.ids.indexOf(key);
      html += '<tr align="right"><td><b>' + key + '</b></td><td style="background-color:' + colorScale(linkNames[key]) + '; color:black;">' + linkNames[key] + '</td>';
      for (md_key in data.md) {
        style = '';
        if (data.selected_md == md_key) {
          background_color = data.colours[strain_idx];
          d3_hcl = d3.hcl(background_color);
          font_color = 'white';
          if (d3_hcl.l > 20) {
            font_color = 'black';
          }
          style = 'style="background-color:' + background_color + '; color:' + font_color + ';"';
        }
        html += '<td ' + style + '>' + data.md[md_key][strain_idx] + '</td>';
      }
      html += '</tr>';
    }
    html += '</table>';
    showTooltip(d, html);
  }

  // Clear any highlighted nodes or links.
  function mouseout() {
    vis.selectAll('.active').classed('active', false);
  }

    
  $(window).resize(initSizes);

  // Make it all go
  this.update(50);
}