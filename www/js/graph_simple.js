<style>

  #tooltip_mst {
  display:none;
  position:absolute;
  top:140px;
  left:900px;
  background-color:#444;
  color:#ddd;
  font-size:10pt;
  text-shadow: none;
  opacity:0.8;
  border-radius:10px;
  padding:5px;
  }

  .node {
    stroke: #fff;
    stroke-width: .5px;
  }

  .link {
    stroke: #999;
    stroke-opacity: 0.6;
  }

  .link.active {
    stroke-width: 1px;
    stroke-opacity: 1;
    stroke: black;
  }
  .link.active text {
    stroke-width: .5px;
    font-size: 22px;
    stroke: white;
  }

  .node.active {
    stroke: red;
    stroke-width: 3px;
  }

</style>
<script src="js/d3.v3.min.js"></script>
<script src='js/myGraph.js'></script>
<script type="text/javascript">

var graph;

var new_plot = true;

var networkOutputBinding = new Shiny.OutputBinding();
$.extend(networkOutputBinding, {
  find: function(scope) {
    return $(scope).find('.shiny-network-output');
  },
  onValueError: function(el, err) {
    exports.unbindAll(el);
    this.renderError(el, err);
  },
  renderValue: function(el, data) {
    console.log('renderValue');
    console.log(data);
    // initialize the graph if it's the first time plotting everything
    if (new_plot) {
      new_plot = false;
      graph = new myGraph(el);
    }
    if (typeof data === 'undefined') return;
    if (data === null) return;
    // add/update nodes in graph
    graph.addNodes(data.ids, data.colours);
    // get links
    var links = [];
    for (var i = 0; i < data.links.length; i++) {
      var x = data.links[i];
      var source = data.ids[x.source];
      var target = data.ids[x.target];
      links.push({'source':source, 'target':target, 'value':x.distances});
    }
    // add all one-way links to graph
    graph.addLinks(links);
    // update graph
    graph.update(data.max_distance, data.show_distances, data);
    
  }
});
Shiny.outputBindings.register(networkOutputBinding, 'cist.networkbinding');


</script>
