# CIST: the Campylobacter *In Silico* Typing analysis tool

This is an R Shiny web app for the integrated analysis and visualization of whole-genome sequence data in combination with strain metadata and *in silico* typing data for Campylobacter. 

## How to get a local version running

- Install [R](http://cran.rstudio.com/)
- Install [RStudio](http://www.rstudio.com/ide/download/) (recommended)
- Have modern browser installed (Chrome, Firefox)
- Download app (link in panel to the right)
- Extract app to a directory
- Launch R/RStudio 
- Navigate to the app directory where you extracted the app archive
- Type (copy-paste) the following into the R Console:

```
install.packages('shiny')
library(shiny)
runApp()
```

- App should launch in your browser
	- if it doesn't, feel free to make an issue in the Issues page with a description of your issue and a copy-paste of your console leading to the issue


## Abstract

### CIST: the Campylobacter *In Silico* Typing server, a resource for integrated comparative genomic analysis of *Campylobacter jejuni* 


Although molecular typing methods have traditionally been used for the characterization of bacterial pathogens, more recently, whole genome sequence (WGS) analysis has emerged as a tool that can be used to address population genetics for public health applications. WGS data can, in principle, resolve bacterial isolates that differ by a single base pair, thus providing the highest level of discriminatory power for epidemiological subtyping.

We present the Campylobacter *In Silico* Typing Server, a bioinformatics resource for rapidly performing a comprehensive suite of analyses from draft genome assemblies. In addition to performing pan-genome analyses that include core genome-based phylogenetic analysis and accessory genome content distribution analysis, this resource integrates the analysis of several DNA-based genotyping schemes that include flaA typing, porA typing, Multi-Locus Sequence Typing, Comparative Genomic Fingerprinting, and AMR profiling. 

In silico genotyping results provide a link between historical typing data and WGS data, while the whole genome phylogenetic analysis provides a framework for the comparison of molecular typing methodologies. In addition, this analysis allows for the identification of genotypic clusters and the exploration of associations between specific genotypes and phenotypic/epidemiologic metadata (e.g., geospatial distribution, host, source) for the identification of group-specific genomic markers (presence/absence of specific genomic regions, and single-nucleotide polymorphisms). 

This platform provides efficient algorithms and pre-computed analyses for the near real-time analyses of thousands of genomic sequences for population genomics, epidemiology, and clinical studies. Collaboration between similar international initiatives will allow for a world-wide real-time surveillance and analyses network for this important pathogen.


