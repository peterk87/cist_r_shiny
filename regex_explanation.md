##### How to use regular expressions

- <a href="http://www.cheatography.com/davechild/cheat-sheets/regular-expressions/" target='_blank'>Regular expressions cheat sheet</a>

Some useful special characters:
- `^` - start of line
	- `^6` matches anything starting with a `6`
- `$` - end of line
	- `55$` matches anything ending with `55`
- `[...]` - match specified characters
	- `[tcm]all` matches `tall`, `call`, `mall`
- `\w` - word character (`0`-`9`, `a`-`z` and `_`)
	- `\weat` matches `[a-z]eat` (e.g. `seat`, `meat`, `peat`, etc)
- `\d` - digit (`0`-`9`)
	- `^1\d$` matches `10`-`19`
- `+` - 1 or more matches of previous character
	- `5+` matches 1 or more `5`s
- `*` - 0 or more matches of previous character
	- `5*` matches 0 or more `5`s
- `|` - OR operator (match this or this)
	- `^21$|^45$` matches `21` or `45`
