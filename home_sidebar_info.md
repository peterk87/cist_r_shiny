#### Links of Interest

- <a href="https://bitbucket.org/peterk87/microbialinsilicotyper" target="mist_gui">Microbial *In Silico* Typer (MIST) (GUI, Windows only)</a>
- <a href="https://bitbucket.org/peterk87/mist" target="mist_cmdline">MIST (cmd line, Windows & Linux)</a>
- <a href="http://darwin.phyloviz.net/ComparingPartitions/" target="ComparingPartitions">Comparing Partitions - Online tool for quantitative assessment of classification agreement</a>

#### Publications of Interest
- <a href="https://www.ncbi.nlm.nih.gov/pubmed/23871858" target="campy_methods">**Current Methods for Molecular Typing of Campylobacter species.** Taboada EN, Clark C, Sproston EL, Carrillo CD. *J Microbiol Methods. 2013 Jul 17. pii: S0167-7012(13)00224-8.*</a>
- <a href="https://www.ncbi.nlm.nih.gov/pubmed/21918028" target="adj_wallace_1">**Adjusted Wallace coefficient as a measure of congruence between typing methods.** Severiano A, Pinto FR, Ramirez M, Carriço JA. *J Clin Microbiol. 2011 Nov;49(11):3997-4000.*</a>