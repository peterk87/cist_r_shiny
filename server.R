
metadata <- read.csv('Cj_338_metadata_mist.csv', header=T, check.names=F, row.names=1, stringsAsFactors=FALSE, colClasses='character')
mat_dm_acc <- as.matrix(read.csv('Cj_338_2391_acc_gene.csv', header=T, check.names=F, row.names=1, stringsAsFactors=FALSE))
mat_dm_core <- as.matrix(read.csv('Cj_338_289_core_gene.csv', header=T, check.names=F, row.names=1, stringsAsFactors=FALSE))
mat_dm_hexmlst <- as.matrix(read.csv('Cj_338_hexmlst_dm.csv', header=T, check.names=F, row.names=1, stringsAsFactors=FALSE))
ref_clusters <- as.matrix(read.csv('Cj_338_strain_SNP_clusters_k_1_to_338.csv', header=T, check.names=F, row.names=1, stringsAsFactors=FALSE))

print('all(rownames(metadata) %in% rownames(mat_dm_core))')
print(all(rownames(metadata) %in% rownames(mat_dm_core)))
print('all(rownames(metadata) %in% rownames(ref_clusters))')
print(all(rownames(metadata) %in% rownames(ref_clusters)))


metadata[metadata == ''] <- '!N/A'


# spectral colours for heatmaps
spectral_colors <- colorRampPalette(RColorBrewer::brewer.pal(n=11, name='Spectral'))

# get cluster order
hc_core <- hclust(as.dist(mat_dm_core))
strains_ordered <- rownames(mat_dm_core)[hc_core$order]

# reorder all data
metadata <- metadata[strains_ordered, ]
mat_dm_core <- mat_dm_core[strains_ordered, strains_ordered]
mat_dm_core_original <- mat_dm_core
mat_dm_hexmlst <- mat_dm_hexmlst[strains_ordered, strains_ordered]
max_core_distance <- max(mat_dm_core)
mat_dm_acc <- mat_dm_acc[strains_ordered, strains_ordered]
ref_clusters <- ref_clusters[strains_ordered,]

qualitative_colours <- function(n, light=FALSE) {
  # Get a specified number of qualitative colours if possible.
  # This function will default to a continuous color scheme if there are more
  # than 21 colours needed.

  # rainbow12equal <- c("#BF4D4D", "#BF864D", "#BFBF4D", "#86BF4D", "#4DBF4D", "#4DBF86", "#4DBFBF", "#4D86BF", "#4D4DBF", "#864DBF", "#BF4DBF", "#BF4D86")
  rich12equal <- c("#000040", "#000093", "#0020E9", "#0076FF", "#00B8C2", "#04E466", "#49FB25", "#E7FD09", "#FEEA02", "#FFC200", "#FF8500", "#FF3300")

  # Qualitative colour schemes by Paul Tol
  ifelse(n >= 19 & n <= 21,
    # return 21 qualitative color scheme
    return(colorRampPalette(c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", "#77AADD", "#117777", "#44AAAA", "#77CCCC", "#117744", "#44AA77", "#88CCAA", "#777711", "#AAAA44", "#DDDD77", "#774411", "#AA7744", "#DDAA77", "#771122", "#AA4455", "#DD7788"))(n)),
  ifelse(n >= 16 & n <= 18,
    # up to 18 qualitative color scheme
    return(colorRampPalette(c("#771155", "#AA4488", "#CC99BB", "#114477", "#4477AA", "#77AADD", "#117777", "#44AAAA", "#77CCCC", "#777711", "#AAAA44", "#DDDD77", "#774411", "#AA7744", "#DDAA77", "#771122", "#AA4455", "#DD7788"))(n)),
  ifelse(n == 15, 
    # 15 qualitative color scheme
    return(colorRampPalette(c("#114477", "#4477AA", "#77AADD", "#117755", "#44AA88", "#99CCBB", "#777711", "#AAAA44", "#DDDD77", "#771111", "#AA4444", "#DD7777", "#771144", "#AA4477", "#DD77AA"))(n)),
  ifelse(n > 12 & n <= 14,
    # 14 qualitative color scheme
    return(colorRampPalette(c("#882E72", "#B178A6", "#D6C1DE", "#1965B0", "#5289C7", "#7BAFDE", "#4EB265", "#90C987", "#CAE0AB", "#F7EE55", "#F6C141", "#F1932D", "#E8601C", "#DC050C"))(n)),
  ifelse(n > 9 & n <= 12,
    ifelse(light,
      return(RColorBrewer::brewer.pal(n=n, name='Set3')),
      return(RColorBrewer::brewer.pal(n=n, name='Paired'))
    ),
  ifelse(n <= 9,
    ifelse(light,
      return(RColorBrewer::brewer.pal(n=n, name='Pastel1')),
      return(RColorBrewer::brewer.pal(n=n, name='Set1'))
    ),
    # else(n > 21,
    # If there are more than 21 qualitative colours, default to a continuous 
    # colour scheme, rich12equal in this case 
    return(colorRampPalette(rich12equal)(n))
  ))))))
}


shinyServer(function(input, output, session) {

  values <- reactiveValues()
  values$mat_dm_core <- mat_dm_core
  values$max_core_distance <- max_core_distance
  observe({
    if (input$select_distances == 'core')
    {
      print('core')
      values$mat_dm_core <- mat_dm_core_original
    }
    else if (input$select_distances == 'accessory')
    {
      print('accessory')
      values$mat_dm_core <- mat_dm_acc
    }
    else if (input$select_distances == 'hexMLST')
    {
      print('hexMLST')
      values$mat_dm_core <- mat_dm_hexmlst
    }
    values$max_core_distance <- max(values$mat_dm_core)
  })

  # the 'grand' analysis ui-element caller
  output$ui_analysis <- renderUI({
    if(input$tool == "dataview") return()
    get(paste('ui_',input$tool, sep=""))()
  })

  # source enabled analysis tools
  flist <- sourceDirectory('tools', recursive = TRUE)
  

  output$jsheatmap <- reactive({
    cat('output$jsheatmap::\n')
    if (length(values$strains) > 300) 
    {
      cat('too many strains (', length(values$strains), ')\n')
      return()
    }
    return(list(strain=values$strains, 
      md=values$metadata_subset, 
      distances=as.vector(values$mat_core_subset), 
      max_distance=max_core_distance))
  })  


  data_col_names <- function(df) {
    cols <- colnames(df)
    # cols <- sort(cols)
    names(cols) <- paste0(cols, " {", sapply(df,class), ";", sapply(cols, function(x) {length(unique(df[[x]]))}), "}")
    cols
  }

  get_cat_group_names <- function(df, category) {
    cat('::get_cat_group_names::', category, '\n')
    print(nrow(df))
    groups <- unique(df[[category]])
    print(groups)
    print(class(groups))
    # groups <- sort(groups, na.last=TRUE)
    cat('any(is.na(groups)): ', any(is.na(groups)), '\n')
    groups[is.na(groups)] <- 'N/A'
    # print(groups)
    # groups <- as.list(groups)
    cat('names(groups)', names(groups), '\n')
    names(groups) <- paste0(groups, ' (n=', sapply(groups, function(x) {
        nrow(df[df[[category]] == x & !(is.na(df[[category]])),])
      }), 
      ')'
    )


    
    group_sizes <- sapply(groups, function(x) {
        nrow(df[df[[category]] == x & !(is.na(df[[category]])),])
      })
    cat('group_sizes: ', group_sizes, '\n')
    size_order <- order(-group_sizes)
    groups <- groups[size_order]
    cat('names(groups)', names(groups), '\n')
    cat('groups', groups, '\n')
    return(groups)
  }

  output$columns <- renderUI({
    df <- metadata_search_subset()
    if(is.null(df)) 
    {
      print('output$columns:: df is NULL')
      return()
    }
    cols <- data_col_names(df)
    if (is.null(cols)) return()
    if (length(cols) == 0) return()
    tagList(
      selectInput("columns", 
        "Select columns to show:", 
        choices  = cols, 
        selected = names(cols)[1:5], 
        multiple = TRUE)
      ,
      tags$style(type='text/css', "#columns { height: 200px;}")
      # ,
      # tags$script('$("#columns").chosen({width: "200px"});')
    )
  })

  # output$nrRows <- renderUI({
  #   # number of observations to show in dataview
  #   df <- metadata_search_subset()
  #   if(is.null(df)) return()
  #   nr <- nrow(df)
  #   print(nr)
  #   sliderInput("nrRows", "Rows to show (max 20):", min = 1, max = nr, value = min(20,nr), step = 1)
  # })

  metadata_search_subset <- reactive({
    print('metadata_search_subset')
    df <- values$metadata_subset
    # return the entire metadata table if values$metadata_subset is null or empty
    if (is.null(df)) 
    {
      print('values$metadata_subset is null. Using entire metadata table')
      values$metadata_subset <- metadata
      df <- values$metadata_subset
    }
    if (length(df) == 0) 
    {
      print('values$metadata_subset is empty. Using entire metadata table.')
      values$metadata_subset <- metadata
      df <- values$metadata_subset
    }

    # if (input$md_view_filter_columns == '' & input$md_view_filter_items == '') 
    # {
    #   print('No filters specified. Using entire metadata table.')
    #   return(metadata)
    # } 

    # filter_columns <- colnames(df)
    # bool_filter_columns <- str_detect(tolower(filter_columns), tolower(input$md_view_filter_columns))
    # print('bool_filter_columns')
    # print(bool_filter_columns)
    # filter_columns <- filter_columns[bool_filter_columns]
    # print('filter_columns')
    # print(filter_columns)
    # df <- as.data.frame(df[ , bool_filter_columns])
    # print(class(df))
    # bool_items <- rep(F, nrow(df))
    # for (col in colnames(df))
    # {
    #   bool_items <- bool_items | (str_detect(tolower(df[[col]]), tolower(input$md_view_filter_items)) & !(is.na(df[[col]])))
    # }
    # print('bool_items')
    # print(bool_items)
    # df <- values$metadata_subset[bool_items, ]
    return(df)
  })

  output$metadata_table <- renderDataTable({
    df <- metadata_search_subset()
    if(is.null(df) || is.null(input$columns)) 
    {
      print('output$dataviewer:: df is NULL or input$columns is NULL')
      return()
    }
    if(!all(input$columns %in% colnames(df))) 
    {
      print('output$dataviewer:: not all input$columns in df')
      return()
    }
    # Show only the selected columns and no more than 20 rows at a time
    # nr <- min(input$nrRows, nrow(df))
    df <- data.frame(df[, input$columns, drop = FALSE])
    cbind(strain=rownames(df), df)
  }, options = list(bSortClasses = TRUE, aLengthMenu = c(5, 10, 25, 50, 100), iDisplayLength = 10))

  output$dl_metadata_subset <- downloadHandler(
    filename = function() { 'metadata_subset.csv' },
    content = function(file)
    {
      write.csv(metadata_search_subset(), file=file)
    }
  )

  # working on fine selection of data to visualize
  output$ui_selection <- renderUI({
    df <- metadata
    cols <- data_col_names(df)

    l <- lapply(cols, function(col){
      d <- tags$div(id=paste0('div_', col))
      label <- tags$label(paste0('Selection for ', col), id=paste0('label_', col))
      d <- tagAppendChild(d, label)
      t <- tags$select(id=paste0('select_', col), multiple=TRUE, style='height:200px; width:200px;')
      factors <- get_cat_group_names(df, col)
      t <- tagAppendChild(t, lapply(names(factors), function(x){tags$option(x, value=factors[x], selected='selected')}))
      d <- tagAppendChild(d, t)
      d
    })
    l
  })

})

